import { configureStore, Action } from '@reduxjs/toolkit'
import { ThunkAction } from 'redux-thunk'
import { routerMiddleware } from 'connected-react-router'
import { createBrowserHistory } from 'history'

import pokedexReducer, { PokedexState } from './reducers'

export const history = createBrowserHistory()

const store = configureStore({
  reducer: pokedexReducer(history),
  middleware: (getDefaultMiddleWare) =>
    getDefaultMiddleWare({
      immutableCheck: false,
      serializableCheck: false
    }).prepend(routerMiddleware(history))
})

export type AppDispatch = typeof store.dispatch

export type AppThunk = ThunkAction<void, PokedexState, null, Action<string>>

export default store
