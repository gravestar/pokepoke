import * as React from 'react'

import './style.scss'

const Loader: React.FC = () => (
  <div className="pokeball-container">
    <div className="pokeball">
      <div className="glow"></div>
      <div className="lower-half"></div>
      <div className="lower-half lh2"></div>
      <div className="circle-out">
        <div className="circle-in"></div>
      </div>
      <div className="shadow"></div>
    </div>
  </div>
)

export default Loader
